# CQSlot

#### 介绍
    
```
C++信号槽独立库

特点：
1、采用现代C++17标准库实现，向下兼容11/14标准，向上兼容未来标准，时尚不落伍
2、模块独立性非常好，直接include头文件即可
3、该框架库天生带有线程安全的特性，使用者不必考虑多线程问题
4、跨平台，无论什么平台都支持（前提：支持C++11编译）
5、封装了线程、定时器、日志等实用模块，定义了大量使用便捷的宏
6、大量使用C++模板、泛型编程，遵循C++面向对象思想，代码风格极简
```



#### 示例

完整案例可以参考main.cpp

```
//根据运行结果：1、某对象如果被移交给了独立线程，则该对象成员函数都在独立线程上跑；否则就在主线程上跑。2、槽函数所在线程与对应信号所在的线程无关。
int main()
{
    // 定义一个独立的线程对象，后面可能用得上
    CQSLOT::CQThread th;
    // 定义完就启动
    th.start();
    std::cout << "任务线程: " << th.getId() << "\n主线程: " << std::this_thread::get_id() << std::endl;

    // 测试类1，我们让它跑在独立线程上
    Test1 test1;
    // 这里意思是我希望目标对象的所有函数在独立线程中跑，而不是在主线程中执行
    test1.moveToThread(&th);

    // 测试类2，我们让它直接在主线程上跑
    Test2 test2;

    // 演示1，定义一个线程信号类，其实不定义也可以，为了演示而已，直接用CQ_EMIT发射信号也可
    ThreadTest tt;
    //把一个信号对象（用CQ_SIGNAL进行定义）绑定到目标槽函数，可以绑n个，它们是“m -- n”的对应关系，非常灵活
    CQ_CONNECT(tt.m_addSignal, &test1, &Test1::add);
    CQ_CONNECT(tt.m_addSignal, &test2, &Test2::reduce);
    // 启动线程，这个线程内部会立即发射信号，信号会立即触发上面connect连接的槽函数
    tt.start(SINGLE); //SINGLE参数表示只执行一次线程循环，不要该参数则默认循环
    std::cout << "信号线程: " << tt.getId() << std::endl;

    // 演示2，定义一个定时器类，定时器定时结束就会发射信号
    CQSLOT::CQTimer timer;
    // 把定时器“定时结束”的信号绑定后面的函数，跟上面一样，“m -- n”的对应关系
    CQ_CONNECT(timer.timeout, &test1, &Test1::timeOut);
    CQ_CONNECT(timer.timeout, &test2, &Test2::timeOut);
    // 定时器开始计时
    timer.start(1000, SINGLE);//SINGLE参数表示只执行一次定时器倒计时，不要该参数则默认循环

    // 开启事件循环
    return CQ_EXEC();
}
```

![输入图片说明](https://images.gitee.com/uploads/images/2021/0629/174058_ccea524e_766467.png "屏幕截图.png")
