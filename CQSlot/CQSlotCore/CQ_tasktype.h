#ifndef CQ_TASKTYPE_H
#define CQ_TASKTYPE_H

#include "CQ_defines.h"

DEFINE_NAMESPACE_CQ_BEGIN

enum CQ_TASK_TYPE
{
    TASK_COMMON = 1 << 0,
    TASK_DELAY = 1 << 1,
};

DEFINE_NAMESPACE_CQ_END

#endif // CQ_TASKTYPE_H
